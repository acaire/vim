runtime bundle/pathogen/autoload/pathogen.vim
let syntastic_puppet_lint_arguments = "--no-80chars-check --fail-on-warnings"
let g:syntastic_python_checker_args = "--max-line-length=160 --disable-msg=C0103"
call pathogen#infect()
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set nocompatible
set foldmethod=syntax
set foldlevelstart=20
set number
colorscheme slate

"Make backspace key work
set backspace=2

if has("syntax")
  syntax on
endif

set background=light

if has("autocmd")
  filetype plugin indent on
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
  autocmd BufWritePost *.py call Flake8()
endif

set showcmd            " Show (partial) command in status line.
set showmatch          " Show matching brackets.
set ignorecase         " Do case insensitive matching
set smartcase          " Do smart case matching
set incsearch          " Incremental search
set autowrite          " Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned
"set mouse=a            " Enable mouse usage (all modes)
set backup
set backupdir=$HOME/.vim/.backup
set directory=$HOME/.vim/.backup
set viewdir=$HOME/.vim/.backup
"set diffopt+=iwhite

" make keypad work in vim with iTerm on OS X!
map! <Esc>Oq 1
map! <Esc>Or 2
map! <Esc>Os 3
map! <Esc>Ot 4
map! <Esc>Ou 5
map! <Esc>Ov 6
map! <Esc>Ow 7
map! <Esc>Ox 8
map! <Esc>Oy 9
map! <Esc>Op 0
map! <Esc>On .
map! <Esc>OQ /
map! <Esc>OR *
map! <kPlus> +
map! <Esc>OS -
map! <Esc>OX =

"Interact with OSX clipboard with CTRL-C and CTRL-V
vmap <C-c> y:call system("pbcopy", getreg("\""))<CR>
vmap <C-v> :call setreg("\"",system("pbpaste"))<CR>p

" Close the Omni-Completion tip window on movement or when leaving insert mode
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" Disable folding for markdown
let g:vim_markdown_folding_disabled=1

" Highlight whitespace at the end of lines
set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.
